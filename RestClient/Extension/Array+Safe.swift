//
// Created by Sergey Suslov on 24.04.2018.
// Copyright (c) 2018 Suslovs. All rights reserved.
//

import Foundation

extension Array {
    subscript(safe index: Index) -> Element? {
        if index >= self.count {
            return nil
        }
        return self[index]
    }
}
