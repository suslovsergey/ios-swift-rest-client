//
// Created by Sergey Suslov on 24.04.2018.
// Copyright (c) 2018 Suslovs. All rights reserved.
//

import Foundation
import Alamofire
import Promises

extension DataRequest {
    @discardableResult
    public func responsePromise(queue: DispatchQueue? = nil) -> Promise<DefaultDataResponse> {
        return Promise<DefaultDataResponse>(on: queue ?? DispatchQueue.main) { fulfill, reject in
            self.response(queue: queue) { (data: DefaultDataResponse) in
                if let error = data.error { return reject(error) }
                fulfill(data)
            }
        }
    }
}
