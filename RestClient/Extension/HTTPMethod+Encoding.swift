//
// Created by Sergey Suslov on 25.04.2018.
// Copyright (c) 2018 Suslovs. All rights reserved.
//

import Foundation
import Alamofire


extension HTTPMethod {
    public var encoding: ParameterEncoding {
        switch self {
        case .patch, .put, .post: return Alamofire.JSONEncoding.default
        default: return Alamofire.URLEncoding.default
        }
    }
}

