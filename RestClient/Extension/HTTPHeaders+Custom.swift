//
// Created by Sergey Suslov on 24.04.2018.
// Copyright (c) 2018 Suslovs. All rights reserved.
//

import Foundation
import Alamofire

extension Dictionary where Key == String, Value == String {
    var userAgent: String? {
        get { return self["User-Agent"] }
        set(newValue) {
            _set("User-Agent", newValue)
        }
    }

    var contentType: String? {
        get { return self["Content-Type"] }
        set(newValue) {
            _set("Content-Type", newValue)
        }
    }

    private mutating func _set(_ key: String, _ value: String?) {
        if let nv = value {
            self[key] = nv
        } else {
            self.removeValue(forKey: key)
        }
    }
}