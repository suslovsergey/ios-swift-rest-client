//
// Created by Суслов Сергей on 23.10.2017.
// Copyright (c) 2017 Webbankir LLC. All rights reserved.
//

import Foundation
import Alamofire

extension Request {
    var httpieDebug: String {
        var components = ["http "]
        var isJson = false

        guard let request = self.request,
              let URL = request.url,
              let _ = URL.host
                else {
            return "$ http command could not be created"
        }

        if let HTTPMethod = request.httpMethod, HTTPMethod != "GET" {
            if HTTPMethod == "POST" || HTTPMethod == "PATCH" || HTTPMethod == "PUT" {
                components.append("-f")
                if let _ = request.allHTTPHeaderFields?.first(where: {
                    $0 == "Content-Type"
                            && ($1 == "application/json; charset=utf-8" || $1 == "application/json")
                }) {
                    isJson = true
                }

            }
            components.append(HTTPMethod)
        }

        components.append("\"\(URL.absoluteString)\"")

        var headers: [AnyHashable: Any] = [:]

        if let additionalHeaders = session.configuration.httpAdditionalHeaders {
            for (field, value) in additionalHeaders where field as! String != "Cookie" {
                headers[field] = value
            }
        }

        if let headerFields = request.allHTTPHeaderFields {
            for (field, value) in headerFields where field != "Cookie" {
                headers[field] = value
            }
        }

        for (field, value) in headers {
            components.append("\(field):'\(value)'")
        }

        if let HTTPBodyData = request.httpBody,
           let HTTPBody = String(data: HTTPBodyData, encoding: String.Encoding.utf8) {

            if isJson {
                components.insert("echo '\(HTTPBody)' |", at: 0)
            } else {
                HTTPBody.replacingOccurrences(of: "\\\"", with: "\\\\\"")
                        .replacingOccurrences(of: "\"", with: "\\\"")
                        .components(separatedBy: "&")
                        .forEach {
                            let p = $0.components(separatedBy: "=")
                            let k = p[safe: 0]?.replacingOccurrences(of: "\'", with: "\\\'").removingPercentEncoding
                            let v = p[safe: 1]?.replacingOccurrences(of: "\'", with: "\\\'").removingPercentEncoding
                            if let k = k, let v = v {
                                components.append("'\(k)'='\(v)'")
                            }
                        }
            }
        }

        return "\n" + components.joined(separator: " \\\n\t") + "\n"
    }
}