//
// Created by Sergey Suslov on 07.03.2018.
// Copyright (c) 2018 Webbankir LLC. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

public struct SPRestClientRouteItem {
    public let path: String
    public let method: HTTPMethod
    public let params: BaseMappable?
    public let cachePolicy: SPRestClientCachePolicy
    public var encoding: ParameterEncoding?
    public var isOutSide: Bool = false

    public init(path: PSPConvertibleToString,
                method: HTTPMethod = .get,
                params: BaseMappable? = nil,
                cachePolicy: SPRestClientCachePolicy = .none,
                encoding: ParameterEncoding? = nil,
                isOutSide: Bool? = false) {
        self.path = path.toString()
        self.method = method
        self.params = params
        self.cachePolicy = .none
        self.encoding = encoding
        if let isOutSide = isOutSide {
            self.isOutSide = isOutSide
        }

    }
}