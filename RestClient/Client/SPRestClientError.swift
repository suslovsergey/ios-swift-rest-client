//
// Created by Sergey Suslov on 01.02.2018.
// Copyright (c) 2018 Webbankir LLC. All rights reserved.
//

import UIKit
import ObjectMapper

// TODO: Check All this errors

public enum SPRestClientError: Error {
    case none
    case userError(SPHttpStatusCode, String?)
    case statusCode(SPHttpStatusCode)
    case noSelf
    case noResponseObject
    case noData
    case canceled
    case error(Error)
    case errorMessage(String?)
    case noJWTToken
    case noResult
    case errorInCasting
    case unauthorized

    public var isUnauthorized: Bool {
        if case .unauthorized = self {
            return true
        } else {
            return false
        }
    }


    public var isWrongStatusCode: Bool {
        if case .statusCode = self {
            return true
        } else {
            return false
        }
    }
}