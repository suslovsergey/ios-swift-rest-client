//
// Created by Sergey Suslov on 24.04.2018.
// Copyright (c) 2018 Suslovs. All rights reserved.
//

import Foundation
import Alamofire

extension SPRestClient {

    func makeHeaders(_ item: SPRestClientRouteItem) -> HTTPHeaders {
        var headers: HTTPHeaders = [:]
        let encoding = item.encoding ?? item.method.encoding

        switch encoding {
        case is Alamofire.URLEncoding:
            switch item.method {
            case .post, .put, .patch: headers.contentType = "application/x-www-form-urlencoded"
            default: headers.contentType = "application/json; charset=utf-8"
            }
        case is Alamofire.JSONEncoding: headers.contentType = "application/json; charset=utf-8"
        default: headers.contentType = "application/json; charset=utf-8"
        }

        guard !item.isOutSide else {
            headers.userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 10_0_1 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) Version/10.0 Mobile/14A403 Safari/602.1"
            return headers
        }

        configuration.headers.custom?.forEach { headers[$0] = $1 }

        headers.userAgent = configuration.headers.userAgent
        if let osVersionField = configuration.headers.osVersionField {
            headers[osVersionField] = UIDevice.current.systemVersion
        }

        if let applicationVersionField = configuration.headers.applicationVersionField,
           let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            headers[applicationVersionField] = version
        }

        guard let cb = configuration.headers.onFly else { return headers }
        cb(item).forEach { (key, value) in headers[key] = value }

        return headers
    }
}