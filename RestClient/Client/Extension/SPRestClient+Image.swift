//
// Created by Sergey Suslov on 01.06.2018.
// Copyright (c) 2018 Suslovs. All rights reserved.
//

import Foundation

import Alamofire
import AlamofireImage
import Promises
import Cache

private let storage = try? Storage(
        diskConfig: DiskConfig(name: "Image", expiry: .seconds(30.0 * 24.0 * 60.0 * 60.0)),
        memoryConfig: MemoryConfig(),
        transformer: TransformerFactory.forCodable(ofType: ImageWrapper.self)
)

// TODO: Add configuration for images

extension SPRestClient {
    public func image(_ url: String) -> Promise<UIImage> {

        return Promise<UIImage>(on: .promises) { fulfill, reject in

            if let cachedImage = (try? storage?.object(forKey: url))??.image {
                fulfill(cachedImage)
            } else {
                DispatchQueue.global(qos: .default).async {
                    Alamofire.request(url).responseImage { response in
                        DispatchQueue.global(qos: .default).async {
                            if let error = response.error {
                                reject(SPRestClientError.error(error))
                            } else if let image = response.result.value?.decompressedImage() {
                                try? storage?.setObject(ImageWrapper(image: image), forKey: url, expiry: .seconds(30.0 * 24.0 * 60.0 * 60.0))
                                fulfill(image)
                            } else {
                                reject(SPRestClientError.noData)
                            }
                        }
                    }
                }
            }
        }
    }
}
