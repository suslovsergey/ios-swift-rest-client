//
// Created by Sergey Suslov on 23.04.2018.
// Copyright (c) 2018 Suslovs. All rights reserved.
//

import Foundation
import ObjectMapper
import Cache

extension SPRestClient {
    func dropCache<T: ImmutableMappable>(_ request: SPRestClientRequest<T>) {
        try? cacheStorageTime?.removeObject(forKey: String(request.hashValue))
    }

    func dropAllCache() {
        try? cacheStorageTime?.removeAll()
    }

    func cache<T: ImmutableMappable>(get request: SPRestClientRequest<T>) -> String? {
        guard let cacheStorageTime = cacheStorageTime else {
            return nil
        }
        var result: String? = nil
        switch request.cachePolicy {
        case .time(_):
            result = try? cacheStorageTime.object(forKey: String(request.hashValue))
        case .none: break
        }

        return result
    }

    func cache<T: ImmutableMappable>(set request: SPRestClientRequest<T>, value: T?) {
        guard let value = value?.toJSONString() else { return }

        switch request.cachePolicy {
        case .time(let seconds):
            try? cacheStorageTime?.setObject(value, forKey: String(request.hashValue), expiry: Expiry.seconds(seconds))
        case .none: break
        }
    }
}