import UIKit
import Alamofire

final class SPRestClientRetrier: RequestRetrier {
    func should(_ manager: SessionManager,
                retry request: Request,
                with error: Error,
                completion: @escaping RequestRetryCompletion) {
        debugPrint("Retrier request \(request.request?.url?.absoluteString ?? ""), attempt: \(request.retryCount + 1)")
        debugPrint(error)
        if request.retryCount < SPRestClient.sharedInstance.configuration.retrierCount {
            completion(true, 0.0)
        } else {
            debugPrint("Retrier failed for request \(request.request?.url?.absoluteString ?? "")")
            completion(false, 0.0)
        }
    }
}
