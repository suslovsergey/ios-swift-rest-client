//
// Created by Sergey Suslov on 23.04.2018.
// Copyright (c) 2018 Suslovs. All rights reserved.
//

import Foundation
import Alamofire

public class SPRestClientConfiguration {
    public var baseURL: String? = nil
    public var isDebug = false
    public var onNetworkActivityStart: SimpleCallFn? = nil
    public var onNetworkActivityStop: SimpleCallFn?
    public var onUnauthorizedAction: SimpleCallFn?
    public var statusesCodeUserError: [Int] = Array(400...499)
    public var retrierCount = 10
    public var timeOut = SPRestClientConfigurationTimeout()
    public var retrier: RequestRetrier?
    public var headers = SPRestClientConfigurationHeaders()

    public init() {}

    public init(_ cb: (SPRestClientConfiguration) -> Void) {
        cb(self)
    }
}

public class SPRestClientConfigurationTimeout {
    public var forRequest: TimeInterval = 30.0
    public var forResource: TimeInterval = 30.0

    public init() {}

    public init(_ cb: (SPRestClientConfigurationTimeout) -> Void) {
        cb(self)
    }
}

public class SPRestClientConfigurationHeaders {
    public var userAgent: String?
    public var contentType: String?
    public var applicationVersionField: String?
    public var osVersionField: String?
    public var custom: HTTPHeaders?
    public var onFly: ((SPRestClientRouteItem) -> HTTPHeaders)?

    public init() {}

    public init(_ cb: (SPRestClientConfigurationHeaders) -> Void) {
        cb(self)
    }
}

public protocol SPPLogging {
    func debug(_ value: Any?)
    func verbose(_ value: Any?)
    func error(_ value: Any?)
    func info(_ value: Any?)
    func warning(_ value: Any?)
}
