import UIKit
import Alamofire
import ObjectMapper
import Promises
import Cache
import SwiftyBeaver

public final class SPRestClient {
    public static var log = SwiftyBeaver.self
    public static let sharedInstance = SPRestClient()
    public var configuration = SPRestClientConfiguration()

    let cacheStorageTime = try? Storage(
            diskConfig: DiskConfig(name: "Time"),
            memoryConfig: MemoryConfig(),
            transformer: TransformerFactory.forCodable(ofType: String.self)
    )

    public lazy var sessionManager: Alamofire.SessionManager! = {
        let cfg = URLSessionConfiguration.default
        cfg.timeoutIntervalForRequest = self.configuration.timeOut.forRequest
        cfg.timeoutIntervalForResource = self.configuration.timeOut.forResource
        cfg.httpCookieAcceptPolicy = .never
        cfg.httpShouldSetCookies = false
        let mng = Alamofire.SessionManager(configuration: cfg)
        mng.retrier = self.configuration.retrier ?? SPRestClientRetrier()
        return mng
    }()

    private init() {}

    public func makeRequest<T: ImmutableMappable>(_ request: SPRestClientRequest<T>) -> DataRequest {
        return self.sessionManager.request(
                SPRestClientUrlConvertible(url: request.url),
                method: request.method,
                parameters: request.parameters,
                encoding: request.encoding,
                headers: request.headers
        )
    }

    public func run<T: ImmutableMappable>(_ request: SPRestClientRequest<T>) -> Promise<SPRestClientResponse<T>> {
        if let cb = configuration.onNetworkActivityStart {
            DispatchQueue.main.async { cb() }
        }

        return Promise<SPRestClientResponse<T>>(on: .promises) { [unowned self] fulfill, reject in
            defer {
                if let cb = self.configuration.onNetworkActivityStop {
                    DispatchQueue.main.async { cb() }
                }
            }

            if let cachedObject = self.cache(get: request), let data = try? Mapper<T>().map(JSONString: cachedObject) {
                fulfill(SPRestClientResponse(
                        result: data,
                        httpStatusCode: .ok,
                        error: nil,
                        timeline: nil,
                        metrics: nil,
                        responseData: nil
                ))
                return
            }

            let req = self.makeRequest(request)

            if self.configuration.isDebug {
                SPRestClient.log.debug(req.httpieDebug)
            }

            let data: DefaultDataResponse = try await(req.responsePromise())

            guard let response = data.response else { throw SPRestClientError.noResponseObject }

            let httpStatusCode: SPHttpStatusCode = SPHttpStatusCode(rawValue: response.statusCode) ?? .unknown

            switch httpStatusCode {
            case .ok, .created: break
            case .unauthorized where self.configuration.onUnauthorizedAction != nil:
                self.configuration.onUnauthorizedAction?()
                reject(SPRestClientError.unauthorized)
                return
            default:
                if self.configuration.statusesCodeUserError.contains(response.statusCode),
                   let dataFromServer = data.data {
                    reject(SPRestClientError.userError(httpStatusCode, String(data: dataFromServer, encoding: .utf8)))
                    return
                }

                reject(SPRestClientError.statusCode(httpStatusCode))
                return
            }

            if let error = data.error {
                reject(SPRestClientError.error(error))
                return
            }

            guard let __data = data.data,
                  let stringValue = String(data: __data, encoding: .utf8) else {
                reject(SPRestClientError.noData)
                return
            }

            let value: T = try Mapper().map(JSONString: stringValue)
            self.cache(set: request, value: value)

            if self.configuration.isDebug {
                SPRestClient.log.debug(data.timeline)
            }

            fulfill(SPRestClientResponse(
                    result: value,
                    httpStatusCode: httpStatusCode,
                    error: data.error,
                    timeline: data.timeline,
                    metrics: data.metrics,
                    responseData: data
            ))
        }

//        return async { [unowned self] in
//            defer {
//                if let cb = self.configuration.onNetworkActivityStop {
//                    DispatchQueue.main.async { cb() }
//                }
//            }
//
//            if let cachedObject = self.cache(get: request), let data = try? Mapper<T>().map(JSONString: cachedObject) {
//                return SPRestClientResponse(
//                        result: data,
//                        httpStatusCode: .ok,
//                        error: nil,
//                        timeline: nil,
//                        metrics: nil,
//                        responseData: nil
//                )
//            }
//
//            let req = self.makeRequest(request)
//
//            if self.configuration.isDebug {
//                SPRestClient.log.debug(req.httpieDebug)
//            }
//
//            let data: DefaultDataResponse = try await(req.responsePromise())
//
//            guard let response = data.response else { throw SPRestClientError.noResponseObject }
//
//            let httpStatusCode: SPHttpStatusCode = SPHttpStatusCode(rawValue: response.statusCode) ?? .unknown
//
//            switch httpStatusCode {
//            case .ok, .created: break
//            case .unauthorized where self.configuration.onUnauthorizedAction != nil:
//                self.configuration.onUnauthorizedAction?()
//                throw SPRestClientError.unauthorized
//            default:
//                if self.configuration.statusesCodeUserError.contains(response.statusCode),
//                   let dataFromServer = data.data {
//                    throw SPRestClientError.userError(httpStatusCode, String(data: dataFromServer, encoding: .utf8))
//                }
//
//                throw SPRestClientError.statusCode(httpStatusCode)
//            }
//
//            if let error = data.error { throw SPRestClientError.error(error) }
//
//            guard let __data = data.data,
//                  let stringValue = String(data: __data, encoding: .utf8) else { throw SPRestClientError.noData }
//
//            let value: T = try Mapper().map(JSONString: stringValue)
//            self.cache(set: request, value: value)
//
//            if self.configuration.isDebug {
//                SPRestClient.log.debug(data.timeline)
//            }
//
//            return SPRestClientResponse(
//                    result: value,
//                    httpStatusCode: httpStatusCode,
//                    error: data.error,
//                    timeline: data.timeline,
//                    metrics: data.metrics,
//                    responseData: data
//            )
//        }
    }

}

