import UIKit

public enum SPHttpStatusCode: Int {
    // 1xx
    case `continue` = 100
    case switchingProtocols = 101
    case processing = 102
    // 2xx
    case ok = 200
    case created = 201
    case accepted = 202
    case nonAAuthoritativeInformation = 203
    case noContent = 204
    case resetContent = 205
    case partialContent = 206
    case multiStatus = 207
    case imUsed = 226
    // 3xx
    case multipleChoices = 300
    case movedPermanently = 301
    case movedTemporarily = 302
    case seeOther = 303
    case notModified = 304
    case useProxy = 306
    case temporaryRedirect = 307
    // 4xx
    case badRequest = 400
    case unauthorized = 401
    case paymentRequired = 402
    case forbidden = 403
    case notFound = 404
    case methodNotAllowed = 405
    case notAcceptable = 406
    case proxyAuthenticationRequired = 407
    case requestTimeout = 408
    case conflict = 409
    case gone = 410
    case lengthRequired = 411
    case preconditionFailed = 412
    case requestEntityTooLarge = 413
    case requestURITooLarge = 414
    case unsupportedMediaType = 415
    case requestedRangeNotSatisfiable = 416
    case expectationFailed = 417
    case unProcessableEntity = 422
    case locked = 423
    case failedDependency = 424
    case unorderedCollection = 425
    case upgradeRequired = 426
    case preconditionRequired = 428
    case tooManyRequests = 429
    case requestHeaderFieldsTooLarge = 431
    case requestedHostUnavailable = 434
    case retryWith = 449
    case unavailableForLegalReasons = 451
    // 5xx
    case internalServerError = 500
    case notImplemented = 501
    case badGateway = 502
    case serviceUnavailable = 503
    case gatewayTimeout = 504
    case httpVersionNotSupported = 505
    case variantAlsoNegotiates = 506
    case insufficientStorage = 507
    case loopDetected = 508
    case bandwidthLimitExceeded = 509
    case notExtended = 510
    case networkAuthenticationRequired = 511
    case unknown = -999

    func isOk() -> Bool {

        return (self == .ok || self == .created)
    }
}
