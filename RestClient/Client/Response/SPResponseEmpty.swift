//
// Created by Sergey Suslov on 24.04.2018.
// Copyright (c) 2018 Suslovs. All rights reserved.
//

import Foundation
import ObjectMapper

public final class SPResponseEmpty: ImmutableMappable {
    public init(map: Map) throws {}
}
