//
// Created by Sergey Suslov on 01.02.2018.
// Copyright (c) 2018 Webbankir LLC. All rights reserved.
//

import UIKit

public enum SPRestClientCachePolicy: Equatable {
    case none
    case time(TimeInterval)
}
