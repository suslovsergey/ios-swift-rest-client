import UIKit
import Alamofire
import ObjectMapper

public  struct SPRestClientResponse<T: ImmutableMappable> {
    public let result: T?
    public let httpStatusCode: SPHttpStatusCode
    public let error: Error?
    public let timeline: Timeline?
    public let metrics: URLSessionTaskMetrics?
    public let responseData: DefaultDataResponse?

    func getResult() throws -> T {
        guard let result = result else { throw SPRestClientError.noResult }
        return result
    }
}
