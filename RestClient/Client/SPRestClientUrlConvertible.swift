//
// Created by Sergey Suslov on 23.04.2018.
// Copyright (c) 2018 Suslovs. All rights reserved.
//

import Foundation
import Alamofire

struct SPRestClientUrlConvertible: URLConvertible {
    let url: String

    func asURL() throws -> URL {
        return URL(string: url)!
    }
}

