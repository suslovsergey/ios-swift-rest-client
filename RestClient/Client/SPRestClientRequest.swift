import UIKit
import Alamofire

private func parametersToString(_ p: [String: Any]) -> String {
    var components: [(String, String)] = []

    for key in p.keys.sorted(by: <) {
        let value = p[key]!
        components += Alamofire.URLEncoding().queryComponents(fromKey: key, value: value)
    }

    return components
            .map { "\($0)=\($1)" }
            .joined(separator: "&")
}

public struct SPRestClientRequest<T>: Hashable {
    let clazz: T.Type
    let method: HTTPMethod
    let encoding: ParameterEncoding
    let url: String
    var parameters: Parameters? = nil
    var headers: HTTPHeaders? = nil
    var cachePolicy: SPRestClientCachePolicy = .none

    public var hashValue: Int {
        var r = method.rawValue.hashValue ^ url.hashValue

        if let headers = headers {
            r ^= parametersToString(headers).hashValue
        }

        if let parameters = parameters {
            r ^= parametersToString(parameters).hashValue
        }
        return r
    }



    public static func ==(lhs: SPRestClientRequest<T>, rhs: SPRestClientRequest<T>) -> Bool {

        var state = false

        if let lp = lhs.parameters, let rp = rhs.parameters {
            state = parametersToString(lp).hashValue == parametersToString(rp).hashValue
        } else if !(lhs.parameters == nil && rhs.parameters == nil) {
            return false
        }

        if let lp = lhs.headers, let rp = rhs.headers {
            state = parametersToString(lp).hashValue == parametersToString(rp).hashValue
        } else if !(lhs.headers == nil && rhs.headers == nil) {
            return false
        }

        return state &&
                lhs.method == lhs.method &&
                lhs.url == lhs.url
    }

}
