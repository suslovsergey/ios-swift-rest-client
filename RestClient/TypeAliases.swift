//
// Created by Sergey Suslov on 23.04.2018.
// Copyright (c) 2018 Suslovs. All rights reserved.
//

import UIKit

public typealias SimpleCallFn = () -> Void