//
// Created by Sergey Suslov on 01.06.2018.
// Copyright (c) 2018 Suslovs. All rights reserved.
//

import UIKit

public protocol PSPConvertibleToString {

    func toString() -> String
}

extension String: PSPConvertibleToString {
    public func toString() -> String {
        return self
    }
}

extension PSPConvertibleToString where Self: RawRepresentable, Self.RawValue == String {
    public func toString() -> String { return rawValue }
}
