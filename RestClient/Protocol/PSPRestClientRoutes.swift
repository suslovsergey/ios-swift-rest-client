//
// Created by Sergey Suslov on 01.03.2018.
// Copyright (c) 2018 Webbankir LLC. All rights reserved.
//

import UIKit
import ObjectMapper
import Promises

public protocol PSPRestClientRoutes {
    func getRouteItem() throws -> SPRestClientRouteItem
}

extension PSPRestClientRoutes {
    @discardableResult
    public func getEmptyResult() throws -> SPResponseEmpty {
        return try getResult(SPResponseEmpty.self)
    }

    public func getResponse<T: ImmutableMappable>(_ clazz: T.Type) throws -> SPRestClientResponse<T> {
        let item = try getRouteItem()
        let client = SPRestClient.sharedInstance
        let request = makeRequest(clazz, item: item, client: client)

        return try await(client.run(request))
    }

    @discardableResult
    public func getResult<T: ImmutableMappable>(_ clazz: T.Type) throws -> T {
        return try getResponse(clazz).getResult()
    }

    public func getURLRequest<T: ImmutableMappable>(_ clazz: T.Type) throws -> URLRequest? {
        let item = try getRouteItem()
        let client = SPRestClient.sharedInstance
        return client.makeRequest(makeRequest(clazz, item: item, client: client)).request
    }

    private func makeRequest<T: ImmutableMappable>(_ clazz: T.Type, item: SPRestClientRouteItem, client: SPRestClient) -> SPRestClientRequest<T> {
        return SPRestClientRequest(
                clazz: clazz,
                method: item.method,
                encoding: item.encoding ?? item.method.encoding,
                url: makeURL(item, client),
                parameters: item.params?.toJSON(),
                headers: client.makeHeaders(item),
                cachePolicy: item.cachePolicy
        )
    }

    private func makeURL(_ item: SPRestClientRouteItem, _ client: SPRestClient) -> String { // FIXME: Move to client

        var url = ""
        if let bp = client.configuration.baseURL {
            url += bp
        }

        if item.path.hasPrefix("https://") || item.path.hasPrefix("http://") {
            url = item.path
        } else {
            url += item.path
        }

        return url
    }
}
